# Pager for Content type

For every node of selected content type module add "previous" and "next" 
buttons to previous or next node.
Module provide custom pager for content type.
Pager will apply to end of noder render array.

There is possibility to use pager for navigating between nodes of same author.
There is a possibility to show titles of the closest nodes before and after a
current node (quantity of nodes shown can be adjusted).

## Example:

let us suppose there are 5 nodes: node 1, node 2, node 3, node4, node 5;
a. current node is 3:
below the pager there will be following links:
node 1, node 2, node 4, node 5

b. current node is 2:
below the pager there will be following links:
node 1, node 3, node 4, node 5

c. current node is 5:
below the pager there will be following links:
node 1, node 2, node 3, node 4

There is hook which makes it possible to control sampling of previous and
next node.
As well as a hook to control sampling of nodes closest to the current one.

## Requirements

Drupal 8.x or 9x

## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).

## Configuration
1. Copy the entire pager_for_content_type directory 
the Drupal `sites/all/modules directory`.

1. Login as an administrator. Enable the module 
in the "Administer" -> "Modules"

1. (Optional) Edit the settings under
`Administer->Configuration->Pager settings->Pager for content type settings`

## Maintainers

- svipsa - [svipsa](https://www.drupal.org/u/svipsa)
- Daniel Schiavone - [schiavone](https://www.drupal.org/u/schiavone)
